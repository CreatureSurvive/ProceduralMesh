﻿using UnityEngine;
using System.Collections.Generic;

namespace MeshGenerator
{   
    public class pm_MeshGenerator
    {
        private List<Vector3> m_Verticies = new List<Vector3> ();
        public List<Vector3> Vertices { get { return m_Verticies; } }

        private List<Vector3> m_Normals = new List<Vector3> ();
        public List<Vector3> Normals { get { return m_Normals; } }

        private List<Vector2> m_UVs = new List<Vector2> ();
        public List<Vector2> UVs { get { return m_UVs; } }

        private List<int> m_Indices = new List<int> ();

        public void AddTrianagle (int index0, int index1, int index3)
        {
            m_Indices.Add (index0);
            m_Indices.Add (index1);
            m_Indices.Add (index3);
        }

        public Mesh GenerateMesh (bool RecalculateNormals = false)
        {
            Mesh mesh = new Mesh ();

            mesh.vertices = m_Verticies.ToArray ();
            mesh.triangles = m_Indices.ToArray ();

            if (m_Normals.Count == m_Verticies.Count)
                mesh.normals = m_Normals.ToArray ();

            if (m_UVs.Count == m_Verticies.Count)
                mesh.uv = m_UVs.ToArray ();

            if (RecalculateNormals)
                mesh.RecalculateNormals ();
            
            mesh.RecalculateBounds ();

            return mesh;
        }
    }
}
