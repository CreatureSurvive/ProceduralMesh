﻿using UnityEngine;
using MeshGenerator;

namespace ProceduralMesh
{
    [RequireComponent (typeof (MeshFilter)),
     RequireComponent (typeof (MeshRenderer))]
    public class pm_Plane : MonoBehaviour
    {
        [SerializeField]
        int m_Width;
        [SerializeField]
        int m_Length;
        [SerializeField]
        int m_Height;
        [SerializeField]
        int m_Segments;

        MeshFilter filter;

        void OnValidate ()
        {
            if (m_Width < 1)
                m_Width = 1;
            if (m_Length < 1)
                m_Length = 1;

            GeneratePlane ();
        }

        void GenerateQuad (pm_MeshGenerator meshGenerator, Vector3 offset)
        {
            meshGenerator.Vertices.Add (new Vector3 (0, 0, 0) + offset);
            meshGenerator.Normals.Add (Vector3.up);
            meshGenerator.UVs.Add (new Vector2 (0, 0));

            meshGenerator.Vertices.Add (new Vector3 (0, 0, m_Length) + offset);
            meshGenerator.Normals.Add (Vector3.up);
            meshGenerator.UVs.Add (new Vector2 (0, 1));

            meshGenerator.Vertices.Add (new Vector3 (m_Width, 0, m_Length) + offset);
            meshGenerator.Normals.Add (Vector3.up);
            meshGenerator.UVs.Add (new Vector2 (1, 1));

            meshGenerator.Vertices.Add (new Vector3 (m_Width, 0, 0) + offset);
            meshGenerator.Normals.Add (Vector3.up);
            meshGenerator.UVs.Add (new Vector2 (1, 0));

            int index = meshGenerator.Vertices.Count - 4;

            meshGenerator.AddTrianagle (index, index + 1, index + 2);
            meshGenerator.AddTrianagle (index, index + 2, index + 3);
        }

        void GenerateQuad (pm_MeshGenerator meshGenerator, Vector3 position, Vector2 uv, bool buildTriangle, int vertsPerRow)
        {
            meshGenerator.Vertices.Add (position);
            meshGenerator.UVs.Add (uv);

            if (buildTriangle)
            {
                int index = meshGenerator.Vertices.Count - 1;

                int index0 = index;
                int index1 = index - 1;
                int index2 = index - vertsPerRow;
                int index3 = index - vertsPerRow - 1;

                meshGenerator.AddTrianagle (index0, index2, index1);
                meshGenerator.AddTrianagle (index2, index3, index1);
            }
        }

        void GeneratePlane ()
        {
            pm_MeshGenerator meshGenerator = new pm_MeshGenerator ();

            for (int l = 0; l <= m_Segments; l++)
            {
                float z = m_Length * l;
                float v = (1.0f / m_Segments) * l;

                for (int w = 0; w <= m_Segments; w++)
                {
                    float x = m_Width * w;
                    float u = (1.0f / m_Segments) * w;

                    Vector3 offset = new Vector3 (x, Random.Range (0.0f, m_Height), z);
                    Vector2 uv = new Vector2 (u, v);

                    bool buildTriangles = l > 0 && w > 0;

                    GenerateQuad (meshGenerator, offset, uv, buildTriangles, m_Segments + 1);
                }
            }

            if (filter == null)
            {
                filter = GetComponent<MeshFilter> ();
            }

            filter.sharedMesh = meshGenerator.GenerateMesh (true);

        }
    }
}
