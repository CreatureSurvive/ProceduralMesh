﻿using UnityEngine;
using MeshGenerator;


namespace ProceduralMesh
{
    [RequireComponent (typeof (MeshFilter)),
     RequireComponent (typeof (MeshRenderer))]
    public class pm_Quad : MonoBehaviour
    {
        [SerializeField]
        int m_Width;
        [SerializeField]
        int m_Length;
        MeshFilter filter;

        void OnValidate ()
        {
            if (m_Width < 1)
                m_Width = 1;
            if (m_Length < 1)
                m_Length = 1;
            GeneratedMesh ();
        }

        void GeneratedMesh ()
        {
            pm_MeshGenerator meshGenerator = new pm_MeshGenerator ();

            meshGenerator.Vertices.Add (new Vector3 (0, 0, 0));
            meshGenerator.Normals.Add (Vector3.up);
            meshGenerator.UVs.Add (new Vector2 (0, 0));

            meshGenerator.Vertices.Add (new Vector3 (0, 0, m_Length));
            meshGenerator.Normals.Add (Vector3.up);
            meshGenerator.UVs.Add (new Vector2 (0, 1));

            meshGenerator.Vertices.Add (new Vector3 (m_Width, 0, m_Length));
            meshGenerator.Normals.Add (Vector3.up);
            meshGenerator.UVs.Add (new Vector2 (1, 1));

            meshGenerator.Vertices.Add (new Vector3 (m_Width, 0, 0));
            meshGenerator.Normals.Add (Vector3.up);
            meshGenerator.UVs.Add (new Vector2 (1, 0));

            meshGenerator.AddTrianagle (0, 1, 2);
            meshGenerator.AddTrianagle (0, 2, 3);

            if (filter == null)
            {
                filter = GetComponent<MeshFilter> ();
            }

            filter = GetComponent<MeshFilter> ();
            filter.sharedMesh = meshGenerator.GenerateMesh ();
        }
    }
}
